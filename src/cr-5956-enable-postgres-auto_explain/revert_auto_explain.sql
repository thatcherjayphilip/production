-- Revert auto_explain to its default (disabled) state.
ALTER SYSTEM RESET auto_explain.sample_rate ;
ALTER SYSTEM RESET auto_explain.log_min_duration ;
ALTER SYSTEM RESET auto_explain.log_analyze ;
ALTER SYSTEM RESET auto_explain.log_timing ;
ALTER SYSTEM RESET auto_explain.log_buffers ;
ALTER SYSTEM RESET auto_explain.log_settings ;
