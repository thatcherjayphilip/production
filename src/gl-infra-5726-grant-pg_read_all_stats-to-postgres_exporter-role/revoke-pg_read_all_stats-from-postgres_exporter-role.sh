#! /usr/bin/env bash

set -euo pipefail

set -x

dry_run="${DRY_RUN:-1}"

echo 'Current permissions for role postgres_exporter:'

sudo gitlab-psql --command="SELECT * FROM information_schema.role_table_grants WHERE table_schema='postgres_exporter' AND grantee='postgres_exporter';"

if [ "${dry_run}" == '0' ]; then
    gitlab-psql --command='REVOKE pg_read_all_stats FROM postgres_exporter;'
else
    echo "[Dry-run] Would have ran command: gitlab-psql --command='REVOKE pg_read_all_stats FROM postgres_exporter;'"
fi
