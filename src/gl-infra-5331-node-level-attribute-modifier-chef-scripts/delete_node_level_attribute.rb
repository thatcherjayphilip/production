#! /usr/bin/env ruby
# frozen_string_literal: false

# For: https://gitlab.com/gitlab-com/gl-infra/production/-/issues/5377

# Remember! The node_attribute_section_name attribute section is being
# deleted from the _"normal"_ attributes, and _not_ the "default"
# attributes. The "default" attributes define the cattle node state.
# Wheras the _"normal"_ attributes define the pet node state. This script
# removes the pet state modifications to revert the node to its cattle
# state.

node_attribute_section_name = 'gitlab_fluentd'

target_node_name = ENV['TARGET_NODE']
dry_run = (ENV['DRY_RUN'] != 'false' && ENV['DRY_RUN'] != 'no')
unless target_node_name.nil? || target_node_name.empty?
  nodes.find(name: target_node_name) do |node|
    if dry_run
      puts "[Dry-run] Would have deleted '#{node_attribute_section_name}'' from normal attributes of node #{node.name}"
    else
      puts "Deleting '#{node_attribute_section_name}'' from normal attributes of node #{node.name}"
      # One should not use the key-value accessor method to check for a
      # nil value here, because chef's VividHash will simply create a new
      # sub-instance for the given key.
      # Reference: https://github.com/chef/chef/blob/2eb1c0ad50fa1680536b8ef78237c188e6cd34ac/lib/chef/node/attribute_collections.rb#L134
      #
      # So, not:
      #
      #   unless node.normal_attrs[node_attribute_section_name].nil?
      #
      # But instead:
      #
      # If the attribute exists:
      if node.normal_attrs.key?(node_attribute_section_name)
        # When one invokes #delete on normal_attrs, one is invoking
        # VividMash#delete, which in turn invokes send_reset_cache(__path__, key)
        # This ultimately proxies to DeepMergeCache#delete, which is finally the
        # proxied Hash.
        # See: https://github.com/chef/chef/blob/2eb1c0ad50fa1680536b8ef78237c188e6cd34ac/lib/chef/node/attribute_collections.rb#L123
        node.normal_attrs.delete(node_attribute_section_name)
        node.save
      end
    end
  end
end
